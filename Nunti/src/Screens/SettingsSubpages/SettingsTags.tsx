import React, { useState, useRef, useContext } from 'react';
import {
    View,
} from 'react-native';

import {
    withTheme,
} from 'react-native-paper';

import { modalRef, snackbarRef, globalStateRef, logRef, fabRef, LanguageContext } from '../../App';
import { Backend } from '../../Backend';
import { Tag } from '../../Backend/Tag'
import EmptyScreenComponent from '../../Components/EmptyScreenComponent'
import Styles from '../../Styles';
import { ScreenProps } from '../../Props.d';
import { useFocusEffect } from '@react-navigation/native';
import ModalWithConfirmation from '../../Components/Modals/ModalWithConfirmation';
import ModalWithTextInput from '../../Components/Modals/ModalWithTextInput';
import SettingsButtonAsItem from '../../Components/SettingsButtons/SettingsButtonAsItem';
import SettingsList from '../../Components/SettingsList';

function SettingsTags(props: ScreenProps) {
    const { lang } = useContext(LanguageContext);

    const [tags, setTags] = useState<Tag[]>([...Backend.UserSettings.Tags]);
    const log = useRef(logRef.current!.globalLog.current.context('SettingsTags'));

    useFocusEffect(
        React.useCallback(() => {
            log.current.debug("feed screen focused, showing fab");
            fabRef.current?.showFab(() => modalRef.current?.showModal(<ModalWithTextInput
                icon='tag' title={lang.add_tags}
                value={lang.tag_name} setValue={addTag}
                confirmText={lang.add} />));

            return () => {
                log.current.debug("feed screen blurred, hiding fab");
                fabRef.current?.hideFab();
            };
        }, [])
    );

    const updateTags = () => {
        setTags([...Backend.UserSettings.Tags]);
    }

    const addTag = async (name: string) => {
        try {
            log.current.debug('Adding tag:', name);

            const tag: Tag = await Tag.New(name);
            snackbarRef.current?.showSnack((lang.added_tag).replace('%tag%', ("\"" + tag.name + "\"")));
            updateTags();
        } catch (err) {
            log.current.error('Can\'t add tag', err);
            snackbarRef.current?.showSnack(lang.add_tag_fail);
        }
    }

    const removeTag = async (tag: Tag) => {
        log.current.debug('Removing tag:', tag.name);

        await Tag.Remove(tag);
        updateTags();

        snackbarRef.current?.showSnack((lang.removed_tag).replace('%tag%',
            ("\"" + tag.name + "\"")), lang.undo, () => {
                Tag.UndoRemove();
                updateTags();
            });

        globalStateRef.current?.reloadFeed(false);
    }

    return (
        <View style={Styles.fabContainer}>
            <SettingsList
                items={tags}
                keyExtractor={(item, _) => item.name}
                itemContent={(item, _) =>
                    <SettingsButtonAsItem icon='tag' title={item.name}
                        secondary={{
                            icon: 'close', onPress: () => modalRef.current?.showModal(<ModalWithConfirmation
                                icon='alert' title={lang.remove_tag} critical={false}
                                description={(lang.remove_confirmation).replace('%item%', ("\"" + item.name + "\""))}
                                confirmText={lang.remove} onConfirm={() => removeTag(item)} />)
                        }}
                    />
                }
                emptyContent={<EmptyScreenComponent title={lang.no_tags}
                    description={lang.no_tags_description} useBottomOffset={false} />}
                theme={props.theme} />
        </View>
    );
}

export default withTheme(React.memo(SettingsTags));
