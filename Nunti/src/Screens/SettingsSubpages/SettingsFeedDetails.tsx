import React, { useState, useRef, useContext } from 'react';
import { View } from 'react-native';

import {
    Chip,
    Card,
    withTheme,
    TextInput,
} from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView } from 'react-native-gesture-handler';

import { modalRef, globalStateRef, logRef, LanguageContext, snackbarRef } from '../../App';
import { Backend } from '../../Backend';
import { Feed, Rule } from '../../Backend/Feed';
import Styles from '../../Styles';
import Text from '../../Components/Text/Text'
import SettingsButton from '../../Components/SettingsButtons/SettingsButton';
import SettingsButtonWithSwitch from '../../Components/SettingsButtons/SettingsButtonWithSwitch';
import { BrowserMode, ScreenProps, ThemeProps } from '../../Props.d';
import { Tag } from '../../Backend/Tag';
import ModalWithRadioButtons from '../../Components/Modals/ModalWithRadioButtons';
import ModalWithTextInput from '../../Components/Modals/ModalWithTextInput';
import SettingsButtonAsItem from '../../Components/SettingsButtons/SettingsButtonAsItem';
import CardTitle from '../../Components/Text/CardTitle';
import SettingsList from '../../Components/SettingsList';
import ModalWithConfirmation from '../../Components/Modals/ModalWithConfirmation';
import ModalWrapper from '../../Components/Modals/ModalWrapper';

function SettingsFeedDetails(props: ScreenProps) {
    const { lang } = useContext(LanguageContext);

    const [feed, _] = useState<Feed>(props.route.params?.feed);
    const [browserMode, setBrowserModeState] = useState(feed.BrowserMode);
    const [feedName, setFeedNameState] = useState(feed.name);
    const [noImages, setNoImagesState] = useState(feed.noImages);
    const [hidden, setHiddenState] = useState(!feed.enabled);
    const [tags, setTagsState] = useState([...feed.tags]);
    const [rules, setRules] = useState<Rule[]>(feed.rules);

    const log = useRef(logRef.current!.globalLog.current.context('SettingsFeedDetails'));

    const setBrowserMode = async (browserMode: 'default' | BrowserMode) => {
        log.current.debug('Changing browser mode:', feed.BrowserMode, '->', browserMode);

        feed.BrowserMode = browserMode;
        setBrowserModeState(feed.BrowserMode);

        await Feed.Save(feed);
    }

    const setFeedName = async (feedName: string) => {
        log.current.debug('Changing feed name:', feed.name, '->', feedName);

        feed.name = feedName;
        setFeedNameState(feed.name);

        await Feed.Save(feed);
        globalStateRef.current?.reloadFeed(true);
    }

    const setNoImages = async () => {
        feed.noImages = !feed.noImages;
        setNoImagesState(feed.noImages);

        await Feed.Save(feed);
        globalStateRef.current?.reloadFeed(true);
    }

    const setHidden = async () => {
        feed.enabled = hidden;
        setHiddenState(!feed.enabled);

        await Feed.Save(feed);
        globalStateRef.current?.reloadFeed(true);
    }

    const setFeedTag = async (tag: Tag) => {
        const newTags = tags;
        if (!tags.some(pickedTag => pickedTag.name == tag.name)) {
            newTags.push(tag);
            Feed.AddTag(feed, tag);
        } else {
            newTags.splice(feed.tags.indexOf(tag), 1);
            Feed.RemoveTag(feed, tag);
        }

        log.current.debug('Changing feed tags:', newTags);

        setTagsState([...newTags]);
        globalStateRef.current?.reloadFeed(false);
    }

    const updateRules = () => {
        setRules([...feed.rules]);
        globalStateRef.current?.reloadFeed(true);
    }

    const addRule = async (rule: Rule) => {
        log.current.debug('Adding rule:', rule.value);

        await Feed.AddRule(feed, rule);

        updateRules();
        snackbarRef.current?.showSnack('Added rule');
    }

    const removeRule = async (rule: Rule) => {
        log.current.debug('Removing rule:', rule.value);

        await Feed.RemoveRule(feed, rule);

        updateRules();

        snackbarRef.current?.showSnack('Removed rule');
        globalStateRef.current?.reloadFeed(true);
    }

    return (
        <SettingsList
            items={rules}
            keyExtractor={(item, _) => `${item.value}${item.isRegex}`}
            itemContent={(item, _) =>
                <SettingsButtonAsItem icon={item.isRegex ? 'regex' : 'text'} title={item.value}
                    secondary={{
                        icon: 'close', onPress: () => modalRef.current?.showModal(<ModalWithConfirmation
                            icon='alert' title={lang.remove_rule} critical={false}
                            description={lang.remove_confirmation.replace('%item%', `"${item.value}"`)}
                            confirmText={lang.remove} onConfirm={() => removeRule(item)} />)
                    }}
                />
            }
            theme={props.theme}
            header={
                <>
                    <Card mode={'contained'} style={Styles.flatListCard}>
                        <View style={[Styles.settingsButton, Styles.settingsRowContainer]}>
                            <View style={Styles.settingsLeftContent}>
                                <Text variant="titleMedium">{lang.feed_status}</Text>
                                <Text variant="labelSmall">{feed.failedAttempts == 0 ? lang.feed_status_ok :
                                    (feed.failedAttempts >= 5 ? lang.feed_status_error : lang.feed_status_warn)}</Text>
                            </View>
                            <Icon name={feed.failedAttempts == 0 ? 'check-circle' : (feed.failedAttempts >= 5 ? 'close-circle' : 'alert')}
                                size={24} color={feed.failedAttempts == 0 ? props.theme.colors.secondary : (feed.failedAttempts >= 5 ? props.theme.colors.error :
                                    props.theme.colors.warn)} />
                        </View>
                    </Card>
                    <Card mode={'contained'} style={Styles.flatListCard}>
                        <SettingsButton title={lang.url} description={feed.url} />
                        <SettingsButton title={lang.feed_name} description={feedName}
                            onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                                title={lang.feed_name} icon='rss'
                                value={feedName} setValue={setFeedName}
                                confirmText={lang.change} />)} />
                    </Card>

                    <Card mode={'contained'} style={Styles.flatListCard}>
                        <SettingsButton title={lang.browser_mode} description={lang[browserMode]}
                            onPress={() => modalRef.current?.showModal(<ModalWithRadioButtons<BrowserMode | 'default'>
                                title={lang.browser_mode} icon='web'
                                value={browserMode} setValue={setBrowserMode}
                                values={['default', 'reader_mode', 'legacy_webview', 'webview', 'external_browser']}
                                theme={props.theme} />)} />
                        <SettingsButtonWithSwitch title={lang.no_images} description={lang.no_images_description}
                            value={noImages} onPress={setNoImages} />
                        <SettingsButtonWithSwitch title={lang.hide_feed} description={lang.hide_feed_description}
                            value={hidden} onPress={setHidden} />
                    </Card>

                    <Card mode={'contained'} style={Styles.flatListCard}>
                        {Backend.UserSettings.Tags.length > 0 ?
                            <View style={[Styles.settingsButton, Styles.chipContainer]}>
                                {Backend.UserSettings.Tags.map((tag) => {
                                    return (
                                        <Chip onPress={() => setFeedTag(tag)} key={tag.name}
                                            selected={tags.some(pickedTag => pickedTag.name == tag.name)}
                                            style={Styles.chip}>{tag.name}</Chip>
                                    );
                                })}
                            </View> : <SettingsButton title={lang.no_tags}
                                description={lang.no_tags_description} />
                        }
                    </Card>

                    <CardTitle padding={false}>{lang.rules}</CardTitle>
                    <Card mode={'contained'} style={Styles.flatListCard}>
                        <SettingsButton title={lang.add_rule} description={lang.add_rule_desc}
                            onPress={() => modalRef.current?.showModal(<AddRuleModal
                                addRule={addRule} theme={props.theme} />)} />
                    </Card>
                </>
            }
        />
    );
}

interface AddRuleModalProps extends ThemeProps {
    addRule: (rule: Rule) => Promise<void>,
}

function AddRuleModal(props: AddRuleModalProps) {
    const { lang } = useContext(LanguageContext);
    const [isRegex, setIsRegex] = useState(false);
    const [inputValue, setInputValue] = useState('');

    return (
        <ModalWrapper icon={isRegex ? 'regex' : 'text'} title={lang.add_rule} outline={true}
            critical={false} contentPadding={true}
            confirmation={{
                title: lang.add, disabled: inputValue == '',
                onPress: async () => await props.addRule(new Rule(inputValue, isRegex))
            }}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={[Styles.settingsModalButton, { paddingBottom: 0 }]}>
                    <TextInput label={lang.keyword} autoCapitalize="none" defaultValue={inputValue}
                        onChangeText={text => setInputValue(text)} />
                </View>

                <View>
                    <SettingsButtonWithSwitch title={lang.regex_rule}
                        description={lang.regex_rule_desc}
                        value={isRegex} onPress={() => setIsRegex(!isRegex)} />
                </View>
            </ScrollView>
        </ModalWrapper>
    );
}

export default withTheme(SettingsFeedDetails);