import React, { useState, useRef, useContext } from 'react';
import { NativeModules } from 'react-native';

import {
    withTheme,
    Card,
} from 'react-native-paper';

import { ScrollView } from 'react-native-gesture-handler';
import * as ScopedStorage from "react-native-scoped-storage";
const NotificationsModule = NativeModules.Notifications;

import { modalRef, snackbarRef, logRef, LanguageContext } from '../../App';
import { Backend } from '../../Backend';
import { ScreenProps } from '../../Props.d';
import Styles from '../../Styles';
import SettingsButtonWithSwitch from '../../Components/SettingsButtons/SettingsButtonWithSwitch';
import SettingsButton from '../../Components/SettingsButtons/SettingsButton';
import ModalWithTextInput from '../../Components/Modals/ModalWithTextInput';

function SettingsBackground(props: ScreenProps) {
    const { lang } = useContext(LanguageContext);

    const [disableBackground, setDisableBackground] = useState(Backend.UserSettings.DisableBackgroundTasks);
    const [backgroundSync, setBackgroundSync] = useState(Backend.UserSettings.EnableBackgroundSync);
    const [notifications, setNotifications] = useState(Backend.UserSettings.EnableNotifications);
    const [notificationInterval, setNotificationInterval] = useState(Backend.UserSettings.NewArticlesNotificationPeriod / 60);
    const [backups, setBackups] = useState(Backend.UserSettings.EnableAutomaticBackups);
    const [backupInterval, setBackupInterval] = useState(Backend.UserSettings.AutomaticBackupPeriod / 24);

    const log = useRef(logRef.current!.globalLog.current.context('SettingsBackground'));

    const toggleDisableBackground = () => {
        setDisableBackground(!disableBackground);

        Backend.UserSettings.DisableBackgroundTasks = !disableBackground;
        Backend.UserSettings.Save();
    }

    const toggleBackgroundSync = () => {
        setBackgroundSync(!backgroundSync);

        Backend.UserSettings.EnableBackgroundSync = !backgroundSync;
        Backend.UserSettings.Save();
    }

    const toggleNotificationsPermissionHelper = async () => {
        if (!(await NotificationsModule.areNotificationsEnabled()) && !notifications) {
            // enabling notifications and permission disabled
            // we pass a callback which gets called after user makes a choice
            log.current.debug("Asking user for notification permission");
            NotificationsModule.getNotificationPermission(toggleNotifications);
        } else {
            toggleNotifications();
        }
    }

    const toggleNotifications = () => {
        setNotifications(!notifications);

        Backend.UserSettings.EnableNotifications = !notifications;
        Backend.UserSettings.Save();
    }

    const toggleBackups = async () => {
        if (!backups) { // automatic backups are being turned on
            const dir = await ScopedStorage.openDocumentTree(true);

            if (dir == null) {
                log.current.warn("User has cancelled turning automatic backups on");
                return;
            } else {
                log.current.info(`Automatic backups enabled, storing in ${dir.uri}`);
                Backend.UserSettings.AutomaticBackupDir = JSON.stringify(dir);
            }
        }

        setBackups(!backups);

        Backend.UserSettings.EnableAutomaticBackups = !backups;
        Backend.UserSettings.Save();
    }

    const setBackupIntervalWrapper = async (interval: string) => {
        const converted = +interval;
        if (Object.is(interval, NaN) || converted < 1) {
            log.current.warn("Changing backup interval failed");
            snackbarRef.current?.showSnack(lang.change_backup_interval_fail);
        } else {
            setBackupInterval(converted);

            Backend.UserSettings.AutomaticBackupPeriod = converted * 24;
            await Backend.UserSettings.Save();

            snackbarRef.current?.showSnack(lang.change_backup_interval_success);
        }
    }

    const setNotificationIntervalWrapper = async (interval: string) => {
        const converted = +interval;
        if (Object.is(converted, NaN) || converted < 1) {
            log.current.warn("Changing notification interval failed");
            snackbarRef.current?.showSnack(lang.change_notification_interval_fail);
        } else {
            setNotificationInterval(converted);

            Backend.UserSettings.NewArticlesNotificationPeriod = converted * 60;
            await Backend.UserSettings.Save();

            snackbarRef.current?.showSnack(lang.change_notification_interval_success);
        }
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButtonWithSwitch title={lang.enable_background} description={lang.enable_background_description}
                    value={!disableBackground} backgroundColor={props.theme.colors.primaryContainer} onPress={toggleDisableBackground} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButtonWithSwitch title={lang.background_sync} description={lang.background_sync_description}
                    value={backgroundSync} disabled={disableBackground} onPress={toggleBackgroundSync} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButtonWithSwitch title={lang.notifications} description={lang.notifications_description}
                    value={notifications} disabled={disableBackground} onPress={toggleNotificationsPermissionHelper} />
                <SettingsButton title={lang.notification_interval}
                    description={(lang.notification_interval_description).replace('%interval%', notificationInterval.toString())}
                    disabled={!notifications || disableBackground}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='bell' title={lang.notification_interval} value={notificationInterval.toString()}
                        suffix={lang.hours} confirmText={lang.add}
                        setValue={setNotificationIntervalWrapper} keyboardType='numeric' />)} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButtonWithSwitch title={lang.automatic_backups} description={lang.automatic_backups_description}
                    value={backups} disabled={disableBackground} onPress={toggleBackups} />
                <SettingsButton title={lang.automatic_backup_interval}
                    description={(lang.backup_interval_description).replace('%interval%', backupInterval.toString())}
                    disabled={!backups || disableBackground}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='backup-restore' title={lang.automatic_backup_interval} value={backupInterval.toString()}
                        suffix={lang.days} confirmText={lang.add}
                        setValue={setBackupIntervalWrapper} keyboardType='numeric' />)} />
            </Card>
        </ScrollView>
    );
}

export default withTheme(React.memo(SettingsBackground));
