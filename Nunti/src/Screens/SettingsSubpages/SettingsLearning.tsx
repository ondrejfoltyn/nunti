import React, { useContext, useState } from 'react';
import {
    withTheme,
    Card
} from 'react-native-paper';

import { ScrollView } from 'react-native-gesture-handler';

import { browserRef, LanguageContext } from '../../App';
import { Backend } from '../../Backend';
import { LearningStatus } from '../../Props.d';
import Styles from '../../Styles';
import { useFocusEffect } from '@react-navigation/native';
import SettingsButton from '../../Components/SettingsButtons/SettingsButton';

function SettingsLearning() {
    const { lang } = useContext(LanguageContext);
    const [learningStatus, setLearningStatus] = useState<LearningStatus>();

    useFocusEffect(
        React.useCallback(() => {
            (async () => {
                setLearningStatus(await Backend.GetLearningStatus());
            })();
        }, [])
    );

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.sorting_status}
                    description={learningStatus?.SortingEnabled ?
                        lang.enabled : (lang.rate_more).replace(
                            '%articles%', learningStatus?.SortingEnabledIn?.toString() ?? Number.NaN.toString())} />
                <SettingsButton title={lang.rated_articles}
                    description={((learningStatus?.TotalUpvotes ?? 0) + (learningStatus?.TotalDownvotes ?? 0)).toString()} />
                <SettingsButton title={lang.rating_ratio}
                    description={learningStatus?.VoteRatio} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.learn_more}
                    onPress={() => browserRef.current?.openExternalBrowser('https://gitlab.com/ondrejfoltyn/nunti/-/wikis/Adaptive-learning')} />
            </Card>
        </ScrollView>
    );
}

export default withTheme(React.memo(SettingsLearning));
