import React, { useState, useRef, useContext } from 'react';
import {
    View,
} from 'react-native';

import {
    withTheme
} from 'react-native-paper';

import { modalRef, snackbarRef, fabRef, globalStateRef, logRef, LanguageContext } from '../../App';
import { Backend } from '../../Backend';
import { Feed } from '../../Backend/Feed';
import Styles from '../../Styles';
import EmptyScreenComponent from '../../Components/EmptyScreenComponent'
import { ScreenProps } from '../../Props.d'; import { useFocusEffect } from '@react-navigation/native';
import ModalWithTextInput from '../../Components/Modals/ModalWithTextInput';
import ModalWithConfirmation from '../../Components/Modals/ModalWithConfirmation';
import SettingsButtonAsItem from '../../Components/SettingsButtons/SettingsButtonAsItem';
import SettingsList from '../../Components/SettingsList';

function SettingsFeeds(props: ScreenProps) {
    const { lang } = useContext(LanguageContext);

    const [feeds, setFeeds] = useState([...Backend.UserSettings.FeedList]);
    const log = useRef(logRef.current!.globalLog.current.context('SettingsFeeds'));

    useFocusEffect(
        React.useCallback(() => {
            log.current.debug("feed screen focused, showing fab");
            fabRef.current?.showFab(() => modalRef.current?.showModal(<ModalWithTextInput
                icon='rss' title={lang.add_feeds}
                value='https://www.website.com/rss' setValue={addFeed}
                confirmText={lang.add} />));

            return () => {
                log.current.debug("feed screen blurred, hiding fab");
                fabRef.current?.hideFab();
            };
        }, [])
    );

    const updateFeeds = () => {
        setFeeds([...Backend.UserSettings.FeedList])
    }

    const addFeed = async (url: string) => {
        log.current.debug('Adding feed:', url);

        try {
            const rssUrl = await Feed.GuessRSSLink(url);
            if (rssUrl == null)
                throw new Error("Cannot guess rss link");

            const feed = await Feed.New(rssUrl);
            updateFeeds();

            snackbarRef.current?.showSnack((lang.added_feed).replace('%feed%', ("\"" + feed.name + "\"")));
            globalStateRef.current?.reloadFeed(true);
        } catch {
            log.current.error('Can\'t add RSS feed');
            snackbarRef.current?.showSnack(lang.add_feed_fail);
        }
    }

    const removeFeed = async (feed: Feed) => {
        log.current.debug('Removing feed:', feed.url);
        await Feed.Remove(feed);

        // snack with undo functionality
        snackbarRef.current?.showSnack((lang.removed_feed).replace('%feed%',
            ("\"" + feed.name + "\"")), lang.undo, () => {
                Feed.UndoRemove();
                updateFeeds();
            });

        updateFeeds();
        globalStateRef.current?.reloadFeed(true);
    }

    return (
        <View style={Styles.fabContainer}>
            <SettingsList
                items={feeds}
                keyExtractor={(item, _) => item.url}
                itemContent={(item, _) =>
                    <SettingsButtonAsItem title={item.name}
                        icon={item.failedAttempts == 0 ? 'rss' : (item.failedAttempts >= 5 ? 'close-circle' : 'alert')}
                        iconColor={item.failedAttempts == 0 ? props.theme.colors.secondary :
                            (item.failedAttempts >= 5 ? props.theme.colors.error : props.theme.colors.warn)}
                        onPress={() => props.navigation.navigate('feed_details', { feed: item })}
                        secondary={{
                            icon: 'close', onPress: () => modalRef.current?.showModal(<ModalWithConfirmation
                                icon='alert' title={lang.remove_feed} critical={false}
                                description={(lang.remove_confirmation).replace('%item%', ("\"" + item.name + "\""))}
                                confirmText={lang.remove} onConfirm={() => removeFeed(item)} />)
                        }}
                    />
                }
                emptyContent={<EmptyScreenComponent title={lang.no_feeds}
                    description={lang.no_feeds_description} useBottomOffset={false} />}
                theme={props.theme} />
        </View>
    );
}

export default withTheme(React.memo(SettingsFeeds));
