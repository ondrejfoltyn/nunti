import React, { useState, useRef, useContext } from 'react';
import {
    withTheme,
    Card,
} from 'react-native-paper';

import { ScrollView } from 'react-native-gesture-handler';

import { modalRef, snackbarRef, globalStateRef, logRef, LanguageContext } from '../../App';
import { Backend } from '../../Backend';
import { ScreenProps } from '../../Props.d';
import Styles from '../../Styles';
import SettingsButton from '../../Components/SettingsButtons/SettingsButton';
import ModalWithConfirmation from '../../Components/Modals/ModalWithConfirmation';
import ModalWithTextInput from '../../Components/Modals/ModalWithTextInput';

function SettingsAdvanced(props: ScreenProps) {
    const { lang } = useContext(LanguageContext);

    const [maxArtAge, setMaxArtAge] = useState(Backend.UserSettings.MaxArticleAgeDays);
    const [discovery, setDiscovery] = useState(Backend.UserSettings.DiscoverRatio * 100);
    const [cacheTime, setCacheTime] = useState(Backend.UserSettings.ArticleCacheTime / 60);
    const [pageSize, setPageSize] = useState(Backend.UserSettings.FeedPageSize);
    const [maxArtFeed, setMaxArtFeed] = useState(Backend.UserSettings.MaxArticlesPerChannel);
    const [artHistory, setArtHistory] = useState(Backend.UserSettings.ArticleHistory);

    const log = useRef(logRef.current!.globalLog.current.context('SettingsAdvanced'));

    const resetCache = async () => {
        snackbarRef.current?.showSnack(lang.reset_art_cache);
        globalStateRef.current?.reloadFeed(true);
    }

    const validateInput = async (value: string, validationFun: (value: number) => boolean, fail_message: string) => {
        const converted = Number(value);
        let fail = false;

        if (Object.is(converted, NaN)) {
            log.current.warn('Input value not a number');
            fail = true;
        } else if (validationFun(converted)) {
            log.current.warn('Input value not allowed');
            fail = true;
        }

        if (fail) {
            snackbarRef.current?.showSnack(fail_message);
            modalRef.current?.hideModal();

            return null;
        } else {
            return converted;
        }
    }

    const saveAdvanced = async (success_message: string) => {
        await Backend.UserSettings.Save();
        snackbarRef.current?.showSnack(success_message);
        modalRef.current?.hideModal();
    }

    const changeMaxArtAge = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 1, lang.change_max_art_age_fail);
        if (converted == null)
            return;

        Backend.UserSettings.MaxArticleAgeDays = converted;

        setMaxArtAge(converted);
        globalStateRef.current?.reloadFeed(false);

        await saveAdvanced(lang.change_max_art_age_success);
    }

    const changeDiscovery = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 0 || value > 100, lang.change_discovery_fail);
        if (converted == null)
            return;

        Backend.UserSettings.DiscoverRatio = converted / 100;

        setDiscovery(converted);
        globalStateRef.current?.reloadFeed(true);

        await saveAdvanced(lang.change_discovery_success);
    }

    const changeCacheTime = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 1, lang.change_cache_time_success);
        if (converted == null)
            return;

        Backend.UserSettings.ArticleCacheTime = converted * 60;

        setCacheTime(converted);
        globalStateRef.current?.reloadFeed(false);

        await saveAdvanced(lang.change_cache_time_success);
    }

    const changeArtHistory = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 1, lang.change_art_history_fail);
        if (converted == null)
            return;

        Backend.UserSettings.ArticleHistory = converted;

        setArtHistory(converted);
        await saveAdvanced(lang.change_art_history_success);
    }

    const changePageSize = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 1, lang.change_page_size_fail);
        if (converted == null)
            return;

        Backend.UserSettings.FeedPageSize = converted;
        Backend.UserSettings.OfflineCacheSize = converted * 2;

        globalStateRef.current?.reloadFeed(false);
        setPageSize(converted);

        await saveAdvanced(lang.change_page_size_success);
    }

    const changeMaxArtFeed = async (value: string) => {
        const converted = await validateInput(value, (value: number) => value < 1, lang.change_max_art_feed_fail);
        if (converted == null)
            return;

        Backend.UserSettings.MaxArticlesPerChannel = converted;

        setMaxArtFeed(converted);
        globalStateRef.current?.reloadFeed(true);

        await saveAdvanced(lang.change_max_art_feed_success);
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.max_art_age} description={lang.max_art_age_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='clock-outline' title={lang.max_art_age}
                        value={maxArtAge.toString()} suffix={lang.days} setValue={changeMaxArtAge}
                        keyboardType='numeric' confirmText={lang.change} />)} />
                <SettingsButton title={lang.discovery} description={lang.discovery_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='book-search' title={lang.discovery}
                        value={discovery.toString()} suffix='%' setValue={changeDiscovery}
                        keyboardType='numeric' confirmText={lang.change} />)} />
                <SettingsButton title={lang.cache_time} description={lang.cache_time_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='timer-off' title={lang.cache_time}
                        value={cacheTime.toString()} suffix={lang.hours} setValue={changeCacheTime}
                        keyboardType='numeric' confirmText={lang.change} />)} />
                <SettingsButton title={lang.art_history} description={lang.art_history_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='history' title={lang.art_history}
                        value={artHistory.toString()} setValue={changeArtHistory}
                        keyboardType='numeric' confirmText={lang.change} />)} />
                <SettingsButton title={lang.page_size} description={lang.page_size_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='arrow-collapse-up' title={lang.page_size}
                        value={pageSize.toString()} setValue={changePageSize}
                        keyboardType='numeric' confirmText={lang.change} />)} />
                <SettingsButton title={lang.max_art_feed} description={lang.max_art_feed_description}
                    onPress={() => modalRef.current?.showModal(<ModalWithTextInput
                        icon='arrow-collapse-up' title={lang.max_art_feed}
                        value={maxArtFeed.toString()} setValue={changeMaxArtFeed}
                        keyboardType='numeric' confirmText={lang.change} />)} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.wipe_cache}
                    onPress={() => modalRef.current?.showModal(<ModalWithConfirmation
                        title={lang.reset_title} description={lang.reset_description}
                        icon='cached' confirmText={lang.reset}
                        onConfirm={resetCache} critical={false} />)} />
            </Card>
        </ScrollView>
    );
}

export default withTheme(React.memo(SettingsAdvanced));
