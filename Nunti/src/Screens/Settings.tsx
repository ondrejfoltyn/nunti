import React, { useState, useRef, useEffect, useContext } from 'react';
import {
    BackHandler,
} from 'react-native';

import {
    withTheme,
    Card,
} from 'react-native-paper';

import * as ScopedStorage from 'react-native-scoped-storage';
import { ScrollView } from 'react-native-gesture-handler';

import { modalRef, snackbarRef, globalStateRef, logRef, LanguageContext } from '../App';
import { Backend } from '../Backend';
import Styles from '../Styles';
import Header from '../Components/Header';

type NavigationParamList = {
    settings_main: undefined,
    tags: undefined,
    feeds: undefined,
    feed_details: undefined,
    background: undefined,
    advanced: undefined,
    learning: undefined,
};

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator<NavigationParamList>();

import SettingsTags from './SettingsSubpages/SettingsTags';
import SettingsFeeds from './SettingsSubpages/SettingsFeeds';
import SettingsFeedDetails from './SettingsSubpages/SettingsFeedDetails';
import SettingsAdvanced from './SettingsSubpages/SettingsAdvanced';
import SettingsBackground from './SettingsSubpages/SettingsBackground';
import SettingsLearning from './SettingsSubpages/SettingsLearning';
import { Backup } from '../Backend/Backup';
import {
    ScreenProps, WindowClassProps, LanguageList,
    BrowserMode, ThemeName, AccentName, LanguageCode, LearningStatus
} from '../Props.d';
import Log from '../Log';
import { useFocusEffect } from '@react-navigation/native';
import SettingsButton from '../Components/SettingsButtons/SettingsButton';
import ModalWithRadioButtons from '../Components/Modals/ModalWithRadioButtons';
import SettingsButtonWithSwitch from '../Components/SettingsButtons/SettingsButtonWithSwitch';
import ModalWithConfirmation from '../Components/Modals/ModalWithConfirmation';

interface Props extends ScreenProps, WindowClassProps {
    languages: LanguageList,
}

function Settings(props: Props) {
    useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if (modalRef.current?.modalVisible) {
                modalRef.current?.hideModal();
                return true;
            } else {
                return false;
            }
        });

        return () => {
            backHandler.remove();
        }
    }, []);

    return (
        <Stack.Navigator
            screenOptions={{
                header: (_props) => <Header windowClass={props.windowClass} canGoBack={_props.navigation.canGoBack}
                    goBack={_props.navigation.goBack} openDrawer={props.navigation.openDrawer} route={_props.route} />,
                animation: 'fade' /* animation slide in from right is too laggy and the default one is very very weird */
            }}>
            <Stack.Screen name="settings_main">
                {_props => <SettingsMain {..._props}
                    languages={props.languages} theme={props.theme} />}
            </Stack.Screen>
            <Stack.Screen name="tags">
                {_props => <SettingsTags {..._props} />}
            </Stack.Screen>
            <Stack.Screen name="feeds">
                {_props => <SettingsFeeds {..._props} />}
            </Stack.Screen>
            <Stack.Screen name="feed_details">
                {_props => <SettingsFeedDetails {..._props} />}
            </Stack.Screen>
            <Stack.Screen name="background">
                {_props => <SettingsBackground {..._props} />}
            </Stack.Screen>
            <Stack.Screen name="advanced">
                {_props => <SettingsAdvanced {..._props} />}
            </Stack.Screen>
            <Stack.Screen name="learning">
                {_props => <SettingsLearning {..._props} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
}

interface SettingsMainProps extends ScreenProps {
    languages: LanguageList,
}

function SettingsMain(props: SettingsMainProps) {
    const { lang } = useContext(LanguageContext);

    const [language, setLanguage] = useState(Backend.UserSettings.Language);
    const [browserMode, setBrowserMode] = useState(Backend.UserSettings.BrowserMode);
    const [disableImages, setDisableImages] = useState(Backend.UserSettings.DisableImages);
    const [wifiOnly, setWifiOnly] = useState(Backend.UserSettings.WifiOnly);
    const [offlineReading, setOfflineReading] = useState(Backend.UserSettings.EnableOfflineReading);

    const [theme, setTheme] = useState(Backend.UserSettings.Theme);
    const [accent, setAccent] = useState(Backend.UserSettings.Accent);
    const [feeds, setFeeds] = useState(Backend.UserSettings.FeedList);
    const [tags, setTags] = useState(Backend.UserSettings.Tags);

    const [learningStatus, setLearningStatus] = useState<LearningStatus>();
    const log = useRef<Log>(logRef.current!.globalLog.current.context('Settings'));

    useFocusEffect(
        React.useCallback(() => {
            (async () => {
                setLearningStatus(await Backend.GetLearningStatus());
            })();

            setFeeds(Backend.UserSettings.FeedList);
            setTags(Backend.UserSettings.Tags);
        }, [])
    );

    const changeLanguage = (language: LanguageCode) => {
        globalStateRef.current!.updateLanguage(language);
        setLanguage(language);
    }

    const changeBrowserMode = (browserMode: BrowserMode) => {
        Backend.UserSettings.BrowserMode = browserMode;
        Backend.UserSettings.Save();

        setBrowserMode(browserMode);
    }

    const changeWifiOnly = () => {
        const newValue = !wifiOnly;

        setWifiOnly(newValue);

        Backend.UserSettings.WifiOnly = newValue;
        Backend.UserSettings.Save();
    }

    const changeOfflineReading = () => {
        const newValue = !offlineReading;

        setOfflineReading(newValue);

        Backend.UserSettings.EnableOfflineReading = newValue;
        Backend.UserSettings.Save();
    }

    const changeDisableImages = () => {
        const newValue = !disableImages;

        setDisableImages(newValue);

        Backend.UserSettings.DisableImages = newValue;
        Backend.UserSettings.Save();
    }

    const changeTheme = async (theme: ThemeName) => {
        setTheme(theme);
        await globalStateRef.current!.updateTheme(theme, Backend.UserSettings.Accent)
    }

    const changeAccent = async (accent: AccentName) => {
        setAccent(accent);
        await globalStateRef.current!.updateTheme(Backend.UserSettings.Theme, accent);
    }

    const importBackup = async () => {
        const file: ScopedStorage.FileType = await ScopedStorage.openDocument(true, 'utf8');
        const allowed_mime = ['text/plain', 'application/octet-stream', 'application/json'];

        if (file == null) {
            log.current.warn('Import cancelled by user');
            return;
        }

        if (allowed_mime.indexOf(file.mime) < 0) {
            snackbarRef.current?.showSnack(lang.import_fail_format);
            return;
        }

        if (await Backup.TryLoadBackup(file.data)) {
            snackbarRef.current?.showSnack(lang.import_ok);

            setLearningStatus(await Backend.GetLearningStatus());

            setLanguage(Backend.UserSettings.Language);
            setBrowserMode(Backend.UserSettings.BrowserMode);
            setDisableImages(Backend.UserSettings.DisableImages);
            setWifiOnly(Backend.UserSettings.WifiOnly);
            setTheme(Backend.UserSettings.Theme);
            setAccent(Backend.UserSettings.Accent);
            setFeeds(Backend.UserSettings.FeedList);
            setTags(Backend.UserSettings.Tags);

            globalStateRef.current?.updateLanguage(Backend.UserSettings.Language);
            globalStateRef.current?.updateTheme(Backend.UserSettings.Theme, Backend.UserSettings.Accent);

            globalStateRef.current?.reloadFeed(true);
        } else {
            snackbarRef.current?.showSnack(lang.import_fail_invalid);
            log.current.error('Import failed');
        }
    }

    const exportBackup = async (exportSettings: boolean) => {
        const backup: string = exportSettings ? await Backup.CreateBackup() : await Backup.ExportOPML();
        const backupFormat: string = exportSettings ? "json" : "opml";

        try {
            const res = await ScopedStorage.createDocument(`NuntiBackup.${backupFormat}`, `application/${backupFormat}`, backup, 'utf8')
            if (res != null)
                snackbarRef.current?.showSnack(lang.export_ok);
        } catch (err) {
            snackbarRef.current?.showSnack(lang.export_fail);
            log.current.error('Failed to export backup. ' + err);
        }
    }

    const resetData = async () => {
        await globalStateRef.current?.resetApp();
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.language} description={lang[language]}
                    onPress={() => modalRef.current?.showModal(
                        <LanguageContext.Consumer>{({ lang }) => <ModalWithRadioButtons<LanguageCode>
                            icon='translate' title={lang.language} value={language}
                            values={['system', ...Object.keys(props.languages)].map(lang => lang as LanguageCode)}
                            setValue={changeLanguage} theme={props.theme} />}
                        </LanguageContext.Consumer>)} />
                <SettingsButton title={lang.browser_mode} description={lang[browserMode]}
                    onPress={() => modalRef.current?.showModal(<ModalWithRadioButtons<BrowserMode>
                        icon='web' title={lang.browser_mode} value={browserMode}
                        values={['webview', 'legacy_webview', 'reader_mode', 'external_browser']}
                        setValue={changeBrowserMode} theme={props.theme} />)} />
                <SettingsButtonWithSwitch title={lang.wifi_only} description={lang.wifi_only_description}
                    value={wifiOnly} onPress={changeWifiOnly} />
                <SettingsButtonWithSwitch title={lang.offline_reading} description={lang.offline_reading_description}
                    value={offlineReading} onPress={changeOfflineReading} />
                <SettingsButtonWithSwitch title={lang.no_images} description={lang.no_images_description}
                    value={disableImages} onPress={changeDisableImages} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.theme} description={lang[theme]}
                    onPress={() => modalRef.current?.showModal(<ModalWithRadioButtons<ThemeName>
                        icon='theme-light-dark' title={lang.theme} value={theme}
                        values={['system', 'light', 'dark', 'black']}
                        setValue={changeTheme} theme={props.theme} />)} />
                <SettingsButton title={lang.accent} description={lang[accent]}
                    onPress={() => modalRef.current?.showModal(<ModalWithRadioButtons<AccentName>
                        icon='palette' title={lang.accent} value={accent}
                        values={['default', 'amethyst', 'aqua', 'cinnamon', 'forest', 'gold', 'ocean', 'orchid', 'material_you']}
                        setValue={changeAccent} theme={props.theme} />)} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.import} onPress={importBackup} />
                <SettingsButton title={lang.export} description={lang.export_desc}
                    onPress={() => exportBackup(true)} />
                <SettingsButton title={lang.export_opml} description={lang.export_opml_desc}
                    onPress={() => exportBackup(false)} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.feeds} description={feeds.length.toString()}
                    onPress={() => props.navigation.navigate('feeds')} />
                <SettingsButton title={lang.tags} description={tags.length.toString()}
                    onPress={() => props.navigation.navigate('tags')} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.learning}
                    description={learningStatus?.SortingEnabled ? lang.enabled :
                        (lang.rate_more).replace('%articles%', learningStatus?.SortingEnabledIn?.toString() ?? Number.NaN.toString())}
                    onPress={() => props.navigation.navigate('learning')} />
                <SettingsButton title={lang.background}
                    description={Backend.UserSettings.DisableBackgroundTasks ? lang.disabled : lang.enabled}
                    onPress={() => props.navigation.navigate('background')} />
                <SettingsButton title={lang.advanced} description={lang.advanced_description}
                    onPress={() => props.navigation.navigate('advanced')} />
            </Card>

            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.wipe_data}
                    critical={true}
                    onPress={() => modalRef.current?.showModal(<ModalWithConfirmation
                        title={lang.restore_title} description={lang.restore_description}
                        icon='alert' confirmText={lang.reset}
                        onConfirm={resetData} critical={true} />)} />
            </Card>
        </ScrollView>
    );
}

export default withTheme(React.memo(Settings));
