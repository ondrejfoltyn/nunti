import React, { useContext, useRef } from 'react';
import {
    View,
    Image,
} from 'react-native';

import {
    withTheme,
    Card
} from 'react-native-paper';

import * as ScopedStorage from 'react-native-scoped-storage';
import { ScrollView } from 'react-native-gesture-handler';
import { version } from '../../package.json';

import { browserRef, snackbarRef, logRef, LanguageContext } from '../App';
import Log from '../Log';

import Styles from '../Styles';
import SettingsButton from '../Components/SettingsButtons/SettingsButton';
import CardTitle from '../Components/Text/CardTitle';

function About() {
    const { lang } = useContext(LanguageContext);
    const log = useRef<Log>(logRef.current!.globalLog.current.context('About'));

    const exportLogs = async () => {
        const logs: string = await Log.exportLogs();

        try {
            if (await ScopedStorage.createDocument('NuntiLogs.txt', 'application/txt', logs, 'utf8') != null) {
                snackbarRef.current?.showSnack(lang.export_logs_success);
            }
        } catch (err) {
            snackbarRef.current?.showSnack(lang.export_logs_fail);
            log.current.error('Failed to export logs, ' + err);
        }
    }

    return (
        <ScrollView>
            <View style={Styles.centeredImageContainer}>
                <Image source={require('../../Resources/HeartNunti.png')}
                    resizeMode="contain" style={Styles.fullscreenImage}></Image>
            </View>

            <CardTitle>{lang.app_info}</CardTitle>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={version} />
            </Card>

            <CardTitle>{lang.made_by}</CardTitle>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title='Richard Klapáč' />
                <SettingsButton title='Ondřej Foltýn' />
            </Card>

            <CardTitle>{lang.report_at}</CardTitle>
            <Card mode={'contained'} style={Styles.card}>
                <SettingsButton title={lang.issue_tracker}
                    onPress={() => browserRef.current?.openExternalBrowser('https://gitlab.com/ondrejfoltyn/nunti/-/issues')} />
                <SettingsButton title={lang.export_logs}
                    onPress={exportLogs} />
            </Card>
        </ScrollView>
    );
}

export default withTheme(React.memo(About));
