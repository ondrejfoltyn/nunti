import { NativeModules } from "react-native";
import Log from "../Log";
import { Utils } from "./Utils";
const NotificationsModule = NativeModules.Notifications;

export class Notifications {
    /* Sends push notification to user, returns true on success, false on fail. */
    public static async Push(message: string, channel: 'new_articles' | 'other', log_: Log = Log.None): Promise<boolean> {
        const log = log_.context('Notifications:Push');
        let channelName: string;
        let channelDescription: string;
        const locale = Utils.GetLocale();
        switch (channel) {
            case 'new_articles':
                channelName = locale.notifications_new_articles;
                channelDescription = locale.notifications_new_articles_description;
                break;
            case 'other':
                channelName = 'Other';
                channelDescription = 'Other notifications';
                break;
            default:
                log.error(`Failed attempt, '${channel}' - channel not allowed.`);
                return false;
        }
        const title = channelName;
        const summary = null;
        const result: true | string = await NotificationsModule.notify(title, message, summary, channelName, channelDescription);
        if (result === true) {
            log.info(`succesfully sent via channel ${channel} ('${message}')`);
            return true;
        } else {
            log.error(`Failed to send '${message}', reason: ${result}`);
            return false;
        }
    }
}