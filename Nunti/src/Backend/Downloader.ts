import { Feed, Rule } from './Feed';
import { Article } from './Article';
import Log from '../Log';
import { UserSettings } from './UserSettings';
import { ArticlesUtils } from './ArticlesUtils';
const DOMParser = require('@xmldom/xmldom').DOMParser; //eslint-disable-line
const XMLSerializer = require('@xmldom/xmldom').XMLSerializer; //eslint-disable-line
import { decode } from 'html-entities';
import { Http } from './Http';

/* Collection of methods for downloading and extracting articles from RSS feeds. */
export class Downloader {
    private static log = Log.BE.context('Downloader');

    /* 
    * Downloads articles from all feeds in feedlist.
    * returns articles and saveToCache, which is false if articles were loaded,
    * but shall not be saved to cache (many feeds unexpectedly failed),
    * for more info see issue #72.
    */
    public static async DownloadArticles(
        abort: AbortController | null = null,
        statusUpdateCallback: ((perctFloat: number) => void) | null = null
    ): Promise<{ articles: Article[], saveToCache: boolean }> {

        const THREADS = 6;
        const log = this.log.context('DownloadArticles');

        log.info('Downloading articles..');
        const timeBegin = Date.now();
        const feedList = UserSettings.Instance.FeedList.slice();

        const arts: Article[] = [];

        let feeds_processed = 0;

        let unexpected_fails = 0;
        let total_fails = 0;

        const statusUpdateWrapper = async (feed: Feed, maxArts: number): Promise<Article[]> => {
            const x = await this.SingleFeed(feed, maxArts);
            feeds_processed += 1;
            if (x.length <= 0) { // feed probably failed
                if (feed.failedAttempts < 4) { // is not marked as faulty
                    unexpected_fails++;
                }
                total_fails++;
            }
            const percentage = (feeds_processed / UserSettings.Instance.FeedList.length);
            if (statusUpdateCallback) statusUpdateCallback(0.75 * percentage);
            return x;
        };

        while (feedList.length > 0) {
            if (abort?.signal.aborted)
                throw new Error('Aborted by AbortController.');
            const promises: Promise<Article[]>[] = [];
            for (let i = 0; i < THREADS; i++) {
                if (feedList.length > 0) {
                    promises.push(statusUpdateWrapper(feedList.splice(0, 1)[0], UserSettings.Instance.MaxArticlesPerChannel));
                }
            }
            const results: Article[][] = await Promise.all(promises);
            for (let i = 0; i < results.length; i++) {
                for (let y = 0; y < results[i].length; y++) {
                    arts.push(results[i][y]);
                }
            }
        }

        const timeEnd = Date.now();
        log.info(`Finished in ${((timeEnd - timeBegin) / 1000)} seconds, got ${arts.length} articles, ${unexpected_fails} unexpected fails.`);

        if (total_fails >= 0.9 * UserSettings.Instance.FeedList.length && UserSettings.Instance.FeedList.length > 0) {
            // more than 90% feeds failed, treat this as a nonfunctioning network and revert failed attempts
            log.error(`Almost all feeds failed (${total_fails}/${UserSettings.Instance.FeedList.length}), possibly non-functioning internet connection.`);
            for (let i = 0; i < UserSettings.Instance.FeedList.length; i++) {
                if (UserSettings.Instance.FeedList[i].failedAttempts > 0) {
                    log.info(`Lowered failedAttempts by 1 - (${UserSettings.Instance.FeedList[i].name})`);
                    UserSettings.Instance.FeedList[i].failedAttempts -= 1;
                }
            }
            await UserSettings.Save();
        }

        if (arts.length > 0)
            ArticlesUtils.ExtractKeywords(arts, (perctFloat: number) => {
                if (statusUpdateCallback) statusUpdateCallback(0.75 + 0.25 * perctFloat);
            }, abort);
        return { articles: arts, saveToCache: unexpected_fails / UserSettings.Instance.FeedList.length <= 0.25 };
    }

    /** Downloads article from a single feed, does not throw errors unless throwError is enabled. */
    public static async SingleFeed(feed: Feed, maxperfeed: number, throwError = false): Promise<Article[]> {
        const log = this.log.context('SingleFeed').context(feed.url);
        if (!feed.enabled) {
            log.debug('(skipped, feed disabled)');
            return [];
        }
        log.debug('Downloading..');

        const startTime = Date.now();
        const arts: Article[] = [];
        let response: string;
        try {
            try {
                response = await Http.Get(feed.url, log, feed.failedAttempts < 2 ? 5000 : 2000); //reduce timeout for feeds which tend to fail anyway
            } catch (err) {
                throw new Error('Cannot read RSS ' + err);
            }
            log.info(`Response in ${Date.now() - startTime} ms.`);

            const parser = new DOMParser({
                locator: {},
                errorHandler: { warning: () => { }, error: () => { }, fatalError: (e: any) => { throw e; } } //eslint-disable-line
            });
            const serializer = new XMLSerializer();

            const xml = parser.parseFromString(response);

            let items: any = null; //eslint-disable-line
            try { if (items === null || items.length == 0) items = xml.getElementsByTagName('channel')[0].getElementsByTagName('item'); } catch { /* dontcare */ } //traditional RSS
            try { if (items === null || items.length == 0) items = xml.getElementsByTagName('feed')[0].getElementsByTagName('entry'); } catch { /* dontcare */ } //atom feeds
            try { if (items === null || items.length == 0) items = xml.getElementsByTagName('item'); } catch { /* dontcare */ } //RDF feeds (https://validator.w3.org/feed/docs/rss1.html)

            if (items === null)
                throw new Error('Cannot parse feed, don\'t know where to find articles. (unsupported feed format?)');

            for (let y = 0; y < items.length; y++) {
                if (arts.length >= maxperfeed)
                    break;
                const item = items[y];
                try {
                    const art = new Article(Math.floor(Math.random() * 1e16));
                    art.source = feed.name;
                    art.sourceUrl = feed.url;
                    art.title = item.getElementsByTagName('title')[0].childNodes[0].nodeValue.substr(0, 256);
                    art.title = decode(art.title, { scope: 'strict' });

                    // fallback for CDATA retards
                    if (art.title.trim() === '') {
                        art.title = serializer.serializeToString(item).match(/title>.*CDATA\[(.*)\]\].*\/title/s)[1].trim();
                        if (art.title.trim() == '')
                            throw new Error(`Got empty title. ${item}`);
                    }

                    this.ParseArticleDescription(art, item, serializer);

                    if (!feed.noImages) {
                        if (art.cover === undefined)
                            this.ParseArticleImage(art, item, serializer);
                    } else
                        art.cover = undefined;

                    this.ParseArticleUrl(art, item);
                    this.ParseArticleDate(art, item);

                    if (this.IsArticleRuleFiltered(art, feed.rules))
                        log.info(`Filter blacklisted article (${art.url}).`);
                    else
                        arts.push(art);
                } catch (err) {
                    log.error(`Cannot process article, err: ${err}`);
                }
            }
            log.info(`Finished download, got ${arts.length} articles, took ${Date.now() - startTime} ms`);

            if (arts.length == 0 && throwError)
                throw new Error('Got 0 articles from this feed.');

            if (feed.failedAttempts != 0) {
                feed.failedAttempts = 0;
                log.info(`reset failedAttempts to ${feed.failedAttempts}`);
                UserSettings.Save();
            }

            return arts;
        } catch (err) {
            log.error('Faulty RSS feed: ', err);
            if (feed.failedAttempts < 9999)
                feed.failedAttempts += 1;
            log.info(`increased failedAttempts to ${feed.failedAttempts}`);
            UserSettings.Save();
            if (throwError)
                throw new Error('Faulty RSS feed ' + err);
            return [];
        }
    }

    /** True if article shall be blacklisted, based on rules. False otherwise. */
    private static IsArticleRuleFiltered(art: Article, rules: Rule[]): boolean {
        return rules.some(rule =>
            rule.isRegex ?
                (art.url.match(new RegExp(rule.value)) || art.title.match(new RegExp(rule.value)) || art.description.match(new RegExp(rule.value)))
                :
                (art.url.includes(rule.value) || art.title.includes(rule.value) || art.description.includes(rule.value))
        );
    }

    private static ParseArticleDescription(art: Article, item: any, serializer: any) { //eslint-disable-line
        try { art.description = item.getElementsByTagName('description')[0].childNodes[0].nodeValue; } catch { /* dontcare */ }
        try { art.description = item.getElementsByTagName('content')[0].childNodes[0].nodeValue; } catch { /* dontcare */ }
        try {
            //fallback for CDATA retards
            if (art.description.trim() === '')
                art.description = serializer.serializeToString(item).match(/description>.*CDATA\[(.*)\]\].*<\/description/s)[1];
        } catch { /* dontcare */ }

        art.description = decode(art.description, { scope: 'strict' });
        art.description = art.description.trim();

        try { art.description = art.description.replace(/<([^>]*)>/g, '').replace(/&[\S]+;/g, '').replace(/\[\S+\]/g, ''); } catch { /* dontcare */ }
        try { art.description = art.description.substr(0, 1024); } catch { /* dontcare */ }
        try {
            art.description = art.description
                .replace(/[^\S ]/, ' ')
                .replace(/[^\S]{3,}/g, ' ')
                // replaces lone surrogates by the replacement character
                // see issue #86
                .replace(/[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/g, '\uFFFD');
        } catch { /* dontcare */ }

    }
    private static ParseArticleImage(art: Article, item: any, serializer: any) { //eslint-disable-line
        try {
            const imagecontent =
                item.getElementsByTagName('enclosure')[0] ||
                item.getElementsByTagName('media:content')[0] ||
                item.getElementsByTagName('media:thumbnail')[0];
            if (
                imagecontent &&
                ((imagecontent.hasAttribute('type') && imagecontent.getAttribute('type').includes('image')) ||
                    (imagecontent.hasAttribute('medium') && imagecontent.getAttribute('medium') === 'image') ||
                    imagecontent.getAttribute('url').match(/\.(?:(?:jpe?g)|(?:png))/i))
            ) {
                art.cover = imagecontent.getAttribute('url');
            }
            // If this first approach did not find an image, try to check the whole 'content:encoded' or if it does not exist the whole 'item'.
            // Checking 'content:encoded' first is necessary, because there are feeds that contain advertisement images outside of 'content:encoded' which would be detected if only the 'item' was checked.
            if (art.cover === undefined) {
                const content = item.getElementsByTagName('content:encoded')[0]
                    ? serializer.serializeToString(item.getElementsByTagName('content:encoded')[0])
                    : serializer.serializeToString(item);
                // Try to match an <img...> tag or &lt;img...&gt; tag. For some reason even with &lt; in the content string, a &gt; is converted to >,
                // perhaps because of the serializer?
                // It needs to be done this way, otherwise feeds with mixed tags and entities cannot be matched properly.
                // And it's also not possible to decode the content already here, because of feeds with mixed tags and entities (they exist...).
                art.cover = content.match(/(<img[\w\W]+?)[/]?(?:>)|(&lt;img[\w\W]+?)[/]?(?:>|&gt;)/i)
                    ? content
                        .match(/(<img[\w\W]+?)[/]?(?:>)|(&lt;img[\w\W]+?)[/]?(?:>|&gt;)/i)[0]
                        .match(/(src=[\w\W]+?)[/]?(?:>|&gt;)/i)[1]
                        .match(/(https?:\/\/[^<>"']+?)[\n"'<]/i)[1]
                    : serializer
                        .serializeToString(item)
                        .match(/(https?:\/\/[^<>"'/]+\/+[^<>"':]+?\.(?:(?:jpe?g)|(?:png)).*?)[\n"'<]/i)[1];
            }
            if (art.cover !== undefined) {
                art.cover = decode(art.cover, { scope: 'strict' }).replace('http://', 'https://');
            }
        } catch { /* dontcare */ }
    }
    private static ParseArticleUrl(art: Article, item: any) { //eslint-disable-line
        const isUrlUnset = () => art.url == 'about:blank' || art.url == null || art.url.trim() == '';

        if (isUrlUnset())
            try { art.url = item.getElementsByTagName('link')[0].childNodes[0].nodeValue; } catch { /* dontcare */ }

        if (isUrlUnset()) // for CDATA cases, see issue #114
            try { art.url = item.getElementsByTagName('link')[0].childNodes[0].nextSibling.nodeValue; } catch { /* dontcare */ }

        if (isUrlUnset()) {
            try {
                const linkElements = item.getElementsByTagName('link');
                if (linkElements.length == 1)
                    art.url = item.getElementsByTagName('link')[0].getAttribute('href');
                else {
                    // Needed for Atom feeds which provide multiple <link>, i.e.: Blogspot
                    // see gitlab issue #53
                    for (let i = 0; i < linkElements.length; i++) {
                        if (linkElements[i].getAttribute('rel') == 'alternate')
                            art.url = linkElements[i].getAttribute('href');
                    }
                }
            } catch { /* dontcare */ }
        }

        if (isUrlUnset()) {
            throw new Error(`Could not find any link to article (title: '${art.title}')`);
        }
    }
    private static ParseArticleDate(art: Article, item: any) { //eslint-disable-line
        const isDateUnset = () => art.date == undefined || art.date == null;
        if (isDateUnset())
            try { art.date = new Date(item.getElementsByTagName('dc:date')[0].childNodes[0].nodeValue); } catch { /* dontcare */ }
        if (isDateUnset())
            try { art.date = new Date(item.getElementsByTagName('pubDate')[0].childNodes[0].nodeValue); } catch { /* dontcare */ }
        if (isDateUnset())
            try { art.date = new Date(item.getElementsByTagName('published')[0].childNodes[0].nodeValue); } catch { /* dontcare */ }
        if (isDateUnset())
            try { art.date = new Date(item.getElementsByTagName('updated')[0].childNodes[0].nodeValue); } catch { /* dontcare */ }
    }
}
