import SanitizeHtml from 'sanitize-html';
import { DOMParser } from 'linkedom';
import { Readability } from '@mozilla/readability';
import HtmlPurifyList from '../HtmlPurifyList';
import { Http } from './Http';
import Log from '../Log';

export type ReadabilityArticle = {
    title: string;
    content: string;
    textContent: string;
    length: number;
    excerpt: string;
    byline: string;
    dir: string;
    siteName: string;
    lang: string;
}

export class WebpageParser {
    /** Extracts readable content from webpage, may throw errors. !Does not respect wifionly mode! */
    public static async ExtractContentAsync(url: string): Promise<ReadabilityArticle> {
        const parser = new DOMParser();
        const log = Log.BE.context("ExtractContentAsync").context(url);

        const doc = parser.parseFromString(SanitizeHtml(await Http.Get(url), {
            allowedTags: HtmlPurifyList.tags,
            allowedAttributes: { '*': HtmlPurifyList.attributes }
        }), 'text/html');

        this.AddBaseTagIntoHead(doc, url);

        const readable: ReadabilityArticle | null = new Readability(doc).parse();
        if (readable == null)
            throw Error('Readability engine extracted nothing.');

        readable.content = this.RemoveImgUrlTrailingSlashes(parser.parseFromString(readable.content), log).childNodes[0].innerHTML;

        return readable;
    }

    /** Adds `<base href="...">` tag into `<head>`, readability expects it */
    private static AddBaseTagIntoHead(doc: HTMLDocument, url: string) {
        const baseElem = doc.createElement('base');
        baseElem.setAttribute('href', url);
        doc.head.appendChild(baseElem);
    }

    /** Fixes up `<img>` tags inside `HTMLDocument` to remove trailing `/` in URLs,
        see issue #118.
        Is sensitive in regards to query parameters.
        Warning: removes trailing slashes even for things like `domain.com/`
        @returns the fixed document reference (original is also modified) */
    private static RemoveImgUrlTrailingSlashes(doc: HTMLDocument, _log: Log = Log.None): HTMLDocument {
        const log = _log.context("removeTrailingSlashes");
        const regex = /\.[a-z]{3,4}\/$/i;

        let tagsAffectedCount = 0;

        const elems = doc.getElementsByTagName("img");
        for (let i = 0; i < elems.length; i++) {
            const elem = elems[i];
            let fullUrl = elem.getAttribute("src");
            if (fullUrl === null) continue;

            // decompose URL and search params
            let url = fullUrl.split('?')[0];
            const params: string | null = fullUrl == url ? null : fullUrl.split('?').slice(1).join("?");

            // check url for trailing slash
            if (regex.test(url)) {
                url = url.substring(0, url.length - 1); // fix
                fullUrl = params === null ? url : url + "?" + params // recombine
                elem.setAttribute("src", fullUrl); // save
                tagsAffectedCount++;
            }
        }
        if (tagsAffectedCount > 0)
            log.debug(`affected ${tagsAffectedCount} tags`);
        return doc;
    }
}