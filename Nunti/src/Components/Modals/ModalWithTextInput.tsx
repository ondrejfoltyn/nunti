import React from "react";
import { useState } from "react";
import { ThemeProps } from "../../Props";
import { TextInput, withTheme } from "react-native-paper";
import { KeyboardTypeOptions } from "react-native";
import ModalWrapper from "./ModalWrapper";

interface Props extends ThemeProps {
    icon: string,
    title: string,
    value: string,
    suffix?: string,
    confirmText: string,
    setValue: (value: string) => Promise<void> | void,
    keyboardType?: KeyboardTypeOptions,
}

function ModalWithTextInput(props: Props) {
    const [value, setValue] = useState(props.value);
    const [inputValue, setInputValue] = useState('');

    const save = async () => {
        const value = inputValue;

        setValue(value);
        await props.setValue(value)
    }

    return (
        <ModalWrapper icon={props.icon} title={props.title} outline={false}
            contentPadding={false} critical={false}
            confirmation={{ title: props.confirmText, disabled: inputValue == '', onPress: save }}>
            <TextInput label={value + (props.suffix ?? '')} autoCapitalize="none"
                keyboardType={props.keyboardType ?? 'default'}
                right={<TextInput.Affix text={props.suffix} />}
                onChangeText={text => setInputValue(text)} />
        </ModalWrapper>
    );
}

export default withTheme(React.memo(ModalWithTextInput));
