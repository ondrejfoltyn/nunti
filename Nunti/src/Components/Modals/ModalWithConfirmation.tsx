import React from "react";
import { ThemeProps } from "../../Props";
import { withTheme } from "react-native-paper";
import Text from "../Text/Text";
import ModalWrapper from "./ModalWrapper";

interface Props extends ThemeProps {
    icon: string,
    title: string,
    description: string,
    confirmText: string,
    onConfirm: () => Promise<void>,
    critical: boolean,
}

function ModalWithConfirmation(props: Props) {
    return (
        <ModalWrapper icon={props.icon} title={props.title} outline={false}
            critical={props.critical} contentPadding={true}
            confirmation={{ title: props.confirmText, disabled: false, onPress: props.onConfirm }}>
            <Text variant="bodyMedium">{props.description}</Text>
        </ModalWrapper>
    );
}

export default withTheme(React.memo(ModalWithConfirmation));
