import React from "react";
import { useState } from "react";
import { ThemeProps, WordIndex } from "../../Props";
import { RadioButton } from "react-native-paper";
import { ScrollView } from "react-native-gesture-handler";
import ModalRadioButton from "../ModalRadioButton";
import ModalWrapper from "./ModalWrapper";

interface Props<T extends WordIndex> extends ThemeProps {
    icon: string,
    title: string,
    value: T,
    values: T[],
    setValue: (value: T) => void,
}

function ModalWithRadioButtons<T extends WordIndex>(props: Props<T>) {
    const [value, setValue] = useState(props.value);

    const save = (value: T) => {
        setValue(value);
        props.setValue(value);
    }

    return (
        <ModalWrapper icon={props.icon} title={props.title} outline={true}
            contentPadding={false} critical={false}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <RadioButton.Group value={value} onValueChange={(value) => save(value as T)}>
                    {props.values.map(value => <ModalRadioButton key={value}
                        value={value} disabled={false}
                        changeValue={newValue => save(newValue as T)} />)}
                </RadioButton.Group>
            </ScrollView>
        </ModalWrapper>
    );
}

export default ModalWithRadioButtons;;
