import React, { useContext } from "react";
import { useState } from "react";
import { ThemeProps } from "../../Props";
import { Button, Dialog, withTheme } from "react-native-paper";
import Styles from "../../Styles";
import { View } from "react-native";
import { LanguageContext, modalRef } from "../../App";

interface Props extends ThemeProps {
    icon: string,
    title: string,
    outline: boolean,
    contentPadding: boolean,
    critical: boolean,
    confirmation?: {
        title: string,
        disabled: boolean,
        onPress: () => Promise<void>,
    },
    cancellation?: {
        title: string,
        onPress: () => Promise<void>,
    },
    children: JSX.Element | JSX.Element[]
}

function ModalWrapper(props: Props) {
    const { lang } = useContext(LanguageContext);
    const [loading, setLoading] = useState(false);

    const confirm = async () => {
        setLoading(true);
        await props.confirmation!.onPress();

        // do not set loading false, looks bad when immediately closing
        modalRef.current?.hideModal();
    }

    let style: any = null;
    if (props.outline && props.contentPadding) {
        style = [Styles.modalScrollArea, {
            borderTopColor: props.theme.colors.outline,
            borderBottomColor: props.theme.colors.outline
        }];
    } else if (props.outline) {
        style = [Styles.modalScrollAreaNoPadding, {
            borderTopColor: props.theme.colors.outline,
            borderBottomColor: props.theme.colors.outline
        }];
    } else {
        style = Styles.modalNonScrollArea;
    }

    return (
        <>
            <Dialog.Icon color={props.critical ? props.theme.colors.error : undefined} icon={props.icon} />
            <Dialog.Title style={Styles.centeredText}>{props.title}</Dialog.Title>
            <View style={style}>
                {props.children}
            </View>
            <View style={Styles.modalButtonContainer}>
                {props.confirmation !== undefined ?
                    <Button textColor={props.critical ? props.theme.colors.error : undefined} onPress={confirm} loading={loading}
                        disabled={loading || props.confirmation.disabled}
                        style={Styles.modalButton}>{props.confirmation.title}
                    </Button>
                    : null}
                <Button onPress={props.cancellation?.onPress ?? modalRef.current?.hideModal}
                    style={Styles.modalButton}>{props.cancellation?.title ?? lang.cancel}</Button>
            </View>
        </>
    );
}

export default withTheme(React.memo(ModalWrapper));
