import React, { useContext } from 'react';

import {
    View,
    TouchableNativeFeedback,
} from 'react-native';

import {
    withTheme,
    RadioButton,
} from 'react-native-paper';

import Styles from '../Styles';
import { ThemeProps, WordIndex } from '../Props.d';
import { LanguageContext } from '../App';
import Text from './Text/Text';

interface Props extends ThemeProps {
    value: WordIndex,
    changeValue: (value: string) => void,
    disabled: boolean,
    name?: string,
}

function ModalRadioButton(props: Props) {
    const { lang } = useContext(LanguageContext);

    return (
        <TouchableNativeFeedback disabled={props.disabled}
            background={TouchableNativeFeedback.Ripple(props.theme.colors.surfaceDisabled, false, undefined)}
            onPress={() => props.changeValue(props.value)}>
            <View style={[Styles.modalRadioButton, Styles.settingsRowContainer]}>
                <RadioButton.Android value={props.value} disabled={props.disabled} />
                <Text variant="bodyLarge" disabled={props.disabled} style={Styles.settingsCheckboxLabel}>
                    {props.name === undefined ? lang[props.value] : props.name}</Text>
            </View>
        </TouchableNativeFeedback>
    );
}

export default withTheme(React.memo(ModalRadioButton));
