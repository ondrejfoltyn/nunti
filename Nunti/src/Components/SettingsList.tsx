import React from 'react';

import {
    FlatList,
    ScrollView,
} from 'react-native';

import {
    Card,
} from 'react-native-paper';

import Styles from '../Styles';
import { ThemeProps } from '../Props';

interface Props<T> extends ThemeProps {
    items: T[],
    keyExtractor: (item: T, index: number) => string,
    itemContent: (item: T, index: number) => JSX.Element,
    emptyContent?: JSX.Element,
    header?: JSX.Element
}

function SettingsList<T>(props: Props<T>) {
    const renderItem = (item: T, index: number) => (
        <Card mode="contained" style={[{ borderRadius: 0 },
        (index == 0) ? Styles.flatListCardTop : undefined,
        (index == props.items.length - 1) ? Styles.flatListCardBottom : undefined]}>
            {props.itemContent(item, index)}
        </Card>
    );

    return (
        <FlatList
            ref={undefined}
            data={props.items}
            keyExtractor={props.keyExtractor}
            showsVerticalScrollIndicator={false}
            removeClippedSubviews={true}
            contentContainerStyle={Styles.fabScrollView}
            renderItem={({ item, index }) => renderItem(item, index)}
            renderScrollComponent={(props) => <ScrollView {...props} />}
            ListEmptyComponent={props.emptyContent}
            ListHeaderComponent={props.header}
        ></FlatList>
    );
}

export default SettingsList;
