import React, { useEffect, useState, useRef, useContext } from 'react';

import {
    View
} from 'react-native';

import {
    SegmentedButtons,
    withTheme
} from 'react-native-paper';

import Backend from '../Backend';
import Styles from '../Styles';
import { ThemeProps, LearningStatus, SortType } from '../Props.d';
import { LanguageContext, snackbarRef } from '../App';

interface Props extends ThemeProps {
    applySorting: (sortingType: SortType) => void,
}

function SortTypeSelector(props: Props) {
    const { lang } = useContext(LanguageContext);
    const [sortType, setSortType] = useState<SortType>();
    const [learningDisabled, setLearningDisabled] = useState<boolean>();

    const learningStatus = useRef<LearningStatus>();

    useEffect(() => {
        (async () => {
            learningStatus.current = await Backend.GetLearningStatus();
            setLearningDisabled(!learningStatus.current.SortingEnabled);

            setSortType(Backend.UserSettings.SortType);
        })();
    }, []);

    useEffect(() => {
        (async () => {
            learningStatus.current = await Backend.GetLearningStatus();
            setLearningDisabled(!learningStatus.current.SortingEnabled);
        })();
    });

    const changeSortType = (newSortType: SortType) => {
        if (newSortType == 'learning' && learningDisabled) {
            const rateMoreLocale = (lang.rate_more).replace('%articles%',
                learningStatus.current?.SortingEnabledIn.toString() ?? Number.NaN.toString())

            snackbarRef.current?.showSnack(rateMoreLocale);
            return;
        }

        if (sortType != newSortType) {
            setSortType(newSortType);
            props.applySorting(newSortType);
        }

        Backend.UserSettings.SortType = newSortType;
        Backend.UserSettings.Save();
    }

    return (
        <View style={Styles.segmentedButtonContainerOutline}>
            <SegmentedButtons value={sortType as SortType}
                onValueChange={changeSortType as (value: string) => void}
                buttons={[
                    {
                        value: 'learning',
                        label: lang.sort_learning,
                        style: [{
                            borderColor: learningDisabled
                                ? props.theme.colors.surfaceDisabled
                                : props.theme.colors.outline
                        }],
                        showSelectedCheck: true
                    },
                    {
                        value: 'date',
                        label: lang.sort_date,
                        showSelectedCheck: true
                    },
                ]}
            />
        </View>
    );
}

export default withTheme(React.memo(SortTypeSelector));
