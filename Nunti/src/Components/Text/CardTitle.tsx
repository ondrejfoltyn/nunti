import React from 'react';
import { withTheme } from 'react-native-paper';
import { ThemeProps } from '../../Props';
import Text from './Text';
import Styles from '../../Styles';

interface Props extends ThemeProps {
    children: string | JSX.Element | JSX.Element[],
    padding?: boolean
}

function CardTitle(props: Props) {
    const style = (props.padding ?? true) ? Styles.settingsSectionTitle : Styles.settingsSectionTitleNoPadding;

    return (
        <Text variant="labelLarge" style={style}>
            {props.children}
        </Text>
    );
}

export default withTheme(React.memo(CardTitle));
