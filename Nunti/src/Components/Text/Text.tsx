import React from 'react';
import { Text as DefaultText, withTheme } from 'react-native-paper';
import { ThemeProps } from '../../Props';
import { VariantProp } from 'react-native-paper/lib/typescript/components/Typography/types';

interface Props extends ThemeProps {
    variant: VariantProp<never>,
    disabled?: boolean,
    children: string | JSX.Element | JSX.Element[],
    style?: any,
    selectable?: boolean,
}

function Text(props: Props) {
    const color = props.disabled ? props.theme.colors.onSurfaceDisabled
        : (props.style?.color ?? props.theme.colors.onSurfaceVariant);

    return (
        <DefaultText variant={props.variant} selectable={props.selectable}
            style={[props.style, { color: color }].flat()}>
            {props.children}
        </DefaultText>
    );
}

export default withTheme(React.memo(Text));
