import React from 'react';
import { View } from 'react-native';
import { withTheme } from 'react-native-paper';
import Styles from '../../Styles';
import { ThemeProps } from '../../Props';
import Text from '../Text/Text';
import SettingsButtonWrapper from './SettingsButtonWrapper';

interface Props extends ThemeProps {
    title: string,
    description?: string,
    backgroundColor?: string,
    disabled?: boolean,
    onPress?: () => void,
    critical?: boolean,
}

function SettingsButton(props: Props) {
    return (
        <SettingsButtonWrapper disabled={props.disabled} onPress={props.onPress}
            backgroundColor={props.backgroundColor}>
            <View style={[Styles.settingsButton]}>
                <Text variant="titleMedium" disabled={props.disabled}
                    style={{ color: props.critical ? props.theme.colors.error : undefined }}>
                    {props.title}
                </Text>
                {props.description !== undefined ? <Text variant="labelSmall" disabled={props.disabled}>
                    {props.description}
                </Text> : null}
            </View>
        </SettingsButtonWrapper>
    )
}

export default withTheme(React.memo(SettingsButton));
