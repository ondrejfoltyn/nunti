import React from 'react';
import { View } from 'react-native';
import { withTheme } from 'react-native-paper';
import Styles from '../../Styles';
import { ThemeProps } from '../../Props';
import Text from '../Text/Text';
import SettingsButtonWrapper from './SettingsButtonWrapper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface Props extends ThemeProps {
    icon: string,
    iconColor?: string,
    title: string,
    onPress?: () => void,
    secondary?: {
        icon: string,
        onPress: () => void,
    }
}

function SettingsButtonAsItem(props: Props) {
    return (
        <SettingsButtonWrapper onPress={props.onPress}>
            <View style={[Styles.settingsButton, Styles.settingsRowContainer]}>
                <View style={[Styles.settingsLeftContent, Styles.settingsRowContainer]}>
                    <Icon style={Styles.settingsIcon} name={props.icon} size={24} color={props.iconColor ?? props.theme.colors.secondary} />
                    <Text variant="titleMedium">{props.title}</Text>
                </View>

                {props.secondary ? <SettingsButtonWrapper onPress={props.secondary?.onPress}>
                    <View style={{ borderLeftWidth: 1, borderLeftColor: props.theme.colors.outline }}>
                        <Icon name={props.secondary?.icon} style={[Styles.settingsIcon, { margin: 12 }]}
                            size={24} color={props.theme.colors.onSurface} />
                    </View>
                </SettingsButtonWrapper> : null}
            </View>
        </SettingsButtonWrapper>
    );
}

export default withTheme(React.memo(SettingsButtonAsItem));
