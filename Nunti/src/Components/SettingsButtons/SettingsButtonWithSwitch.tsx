import React from 'react';
import { View } from 'react-native';
import { withTheme } from 'react-native-paper';
import Switch from '../Switch';
import Styles from '../../Styles';
import { ThemeProps } from '../../Props';
import Text from '../Text/Text';
import SettingsButtonWrapper from './SettingsButtonWrapper';

interface Props extends ThemeProps {
    title: string,
    description?: string,
    value: boolean,
    backgroundColor?: string,
    disabled?: boolean,
    onPress?: () => void,
}

function SettingsButtonWithSwitch(props: Props) {
    return (
        <SettingsButtonWrapper disabled={props.disabled} onPress={props.onPress}
            backgroundColor={props.backgroundColor}>
            <View style={[Styles.settingsButton, Styles.settingsRowContainer]}>
                <View style={Styles.settingsLeftContent}>
                    <Text variant="titleMedium" disabled={props.disabled}>
                        {props.title}
                    </Text>
                    {props.description !== undefined ? <Text variant="labelSmall" disabled={props.disabled}>
                        {props.description}
                    </Text> : null}
                </View>
                <Switch value={props.value} disabled={props.disabled ?? false} />
            </View>
        </SettingsButtonWrapper>
    );
}

export default withTheme(React.memo(SettingsButtonWithSwitch));
