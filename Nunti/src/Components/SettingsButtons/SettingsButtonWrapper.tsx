import React from 'react';
import { withTheme } from 'react-native-paper';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { ThemeProps } from '../../Props';
import { View } from 'react-native';

interface Props extends ThemeProps {
    disabled?: boolean,
    onPress?: () => void,
    backgroundColor?: string,
    children: JSX.Element | JSX.Element[]
}

function SettingsButtonWrapper(props: Props) {
    const backgroundColor = props.disabled ? props.theme.colors.surfaceDisabled :
        props.backgroundColor ?? 'transparent';

    return (
        <TouchableNativeFeedback
            background={TouchableNativeFeedback.Ripple(props.theme.colors.surfaceDisabled, false, undefined)}
            disabled={props.disabled || !props.onPress} onPress={props.onPress}>
            <View style={{ backgroundColor: backgroundColor }}>
                {props.children}
            </View>
        </TouchableNativeFeedback>
    );
}

export default withTheme(React.memo(SettingsButtonWrapper));
